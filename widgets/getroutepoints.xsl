<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:gpx="http://www.topografix.com/GPX/1/0"
  xmlns:topografix="http://www.topografix.com/GPX/Private/TopoGrafix/0/1"
  >
  <xsl:output method="text" version="1.0" encoding="ISO-8859-1" indent="no"
    omit-xml-declaration="yes"  media-type="text/plain" standalone="yes" />
  <xsl:template match="/">
    [
    <xsl:for-each select="gpx:gpx/gpx:rte">
      <xsl:if test="gpx:number = $routenumber">
        <xsl:for-each select="gpx:rtept">
          <xsl:if test="position() &gt; 1">
          ,
          </xsl:if>
          {
            x : "<xsl:value-of select="@lon" />"
          , y : "<xsl:value-of select="@lat" />"
          , ele : "<xsl:value-of select="gpx:ele"/>"
          , name : "<xsl:value-of select="gpx:name"/>"
          , desc : "<xsl:value-of select="gpx:desc"/>"
          , sym : "<xsl:value-of select="gpx:sym"/>"
          , time : "<xsl:value-of select="gpx:time"/>"
          , type : "<xsl:value-of select="gpx:type"/>"
          }
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
    ]
  </xsl:template>
  
<xsl:template name="escape-javascript">
  <xsl:param name="string" />
  <xsl:choose>
    <xsl:when test='contains($string, "&apos;")'>
      <xsl:call-template name="escape-javascript">
        <xsl:with-param name="string"
          select='substring-before($string, "&apos;")' />
      </xsl:call-template>
      <xsl:text>\'</xsl:text>
      <xsl:call-template name="escape-javascript">
        <xsl:with-param name="string"
          select='substring-after($string, "&apos;")' />
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="contains($string, '&#xA;')">
      <xsl:call-template name="escape-javascript">
        <xsl:with-param name="string"
          select="substring-before($string, '&#xA;')" />
      </xsl:call-template>
      <xsl:text>\n</xsl:text>
      <xsl:call-template name="escape-javascript">
        <xsl:with-param name="string"
          select="substring-after($string, '&#xA;')" />
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="contains($string, '\')">
      <xsl:value-of select="substring-before($string, '\')" />
      <xsl:text>\\</xsl:text>
      <xsl:call-template name="escape-javascript">
        <xsl:with-param name="string"
          select="substring-after($string, '\')" />
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="$string" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
