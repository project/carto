
function CartoGPXFiles()
{
    // inheritance
    Base.apply( this, arguments );
}

function CartoGPXFiles_init()
{
    // call overloaded function
    Base.prototype.init.apply( this, arguments );
    
    this.select = MappingWidgets.browser.getElement( this.id + '-select' );
    
    this.select.onchange = function()
    {
        var widget = this.parentNode.mappingwidget;
        var mapwidget = this.parentNode.mappingwidget.findMapWidget();
        if( mapwidget )
        {
            // filename|waypoints
            // or filename|track|nr
            
            // add points to default canvas
            var canvas = mapwidget.getCanvas( 'default' );
            canvas.reset();
            var points = widget.getPoints();
            canvas.addPoints( points );
            
            // change extent?
            var option = this.value.split('|');
            var extent = getBoundingRect( points );
            //alert( extent );
            mapwidget.set( 'extent', extent );
        }
    }
}

CartoGPXFiles.prototype = new Base();
CartoGPXFiles.prototype.init = CartoGPXFiles_init;

function isFunction(a) { return typeof(a) == 'function'; }

function isNull(a) { return typeof(a) == 'object' && !a; }

function isObject(a) { return (a && typeof(a) == 'object') || isFunction(a); }

//function isUndefined(a) { return typeof(a) == 'undefined'; }

//function isDefined(a) { return ! isUndefined(a); }

function isString(a) { return typeof a == 'string'; }

function isArray(a) { return isObject(a) && a.constructor == Array; }

function CartoGPXFiles_getPoints( )
{
    var url = drupal_base_url + '/modules/carto/widgets/CartoGPXFiles.php?';
    var option = this.select.value.split('|');
    url += 'filename=' + option[0];
    if( option[1] == 'waypoints' )
    {
        url += '&action=getwaypoints';
    }
    else if( option[1] == 'trackpoints' )
    {
        url += '&action=gettrackpoints';
        url += '&track=' + option[2];
    }
    else if( option[1] == 'routepoints' )
    {
        url += '&action=getroutepoints';
        url += '&route=' + option[2];
    }
    url += '&nid=' + gpx_nid;
    
    var response = HTTPGet( url );
alert( url + '\n' + response );

// next line hangs
//    var tracks = JSON.parse( response );
// next line works
    if( response != '' )
    {
        try
        {
            eval( 'var points = ' + response + ';' );
            return points;
        }
        catch( e )
        {
            alert( e + '\n' + url + '\n' + response + '/' + typeof(response) );
        }
    }
}

CartoGPXFiles.prototype.getPoints = CartoGPXFiles_getPoints;

function getBoundingRect( points )
{
    if( points.length <= 0 )
        return;
    
    var ext = new Extent( Number.MAX_VALUE, Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE);
    for( var i = 0; i < points.length; i++ )
    {
        var x = parseFloat( points[i].x );
        var y = parseFloat( points[i].y );
        if( x < ext.min.x ) ext.min.x = x;
        if( y < ext.min.y ) ext.min.y = y;
        if( x > ext.max.x ) ext.max.x = x;
        if( y > ext.max.y ) ext.max.y = y;
    }
    return ext;
}

