<?php

/**
 * GPX
 */

class GPX
{
    var $parser;
    var $version;
    var $root = NULL; // root element name (depends on wms version)
    var $path = ''; // current path (excluding root)
    var $element = ''; // current element
    var $bbox;
    var $data;
    var $routes = array();
    var $tracks = array();
    var $currentType;
    var $currentItem;
    
    function GPX( )
    {
        $this->parser = xml_parser_create();
        xml_parser_set_option( $this->parser, XML_OPTION_CASE_FOLDING, 0 );
        xml_set_element_handler( $this->parser, array(&$this,"_startElement"), array(&$this,"_endElement") );
        xml_set_character_data_handler( $this->parser, array(&$this,"_characterData") );
    }
    
    function load( $file )
    {
        if (!($fp = fopen($file, "r")))
        {
            die("could not open XML input");
        }

        $this->data = fread($fp, filesize($file));
        fclose($fp);
        return;
        
        $dom = new DOMDocument();
        $dom->validateOnParse = true;
        $dom->load( $path );
        
        // get bbox
        $node = $dom->documentElement->getElementsByTagName( 'bounds' )->item(0);
        $bbox = array( $node->getAttribute( 'minlon' )
            , $node->getAttribute( 'minlat' )
            , $node->getAttribute( 'maxlon' )
            , $node->getAttribute( 'maxlat' )
            );
    }
    
    function free( )
    {
        xml_parser_free( $this->parser );
    }
    
    function parse( )
    {
        return xml_parse( $this->parser, $this->data, TRUE );
    }
    
    function _startElement( $parser, $name, $attrs )
    {
        if( ! $this->root )
        {
            $this->root = $name;
            $this->version = $attrs['version'];
        }
        else
        {
            $this->path .= '/' . $name;
        }
        $this->element = $name;
        
        switch( $name )
        {
            case 'bounds':
                $this->bbox = $attrs['minlon'] . ',' . $attrs['minlat'] 
                    . ',' . $attrs['maxlon'] . ',' . $attrs['maxlat'];
                break;
            
            case 'wpt': // waypoint
                $this->currentType = 'wpt';
                $this->currentItem = array();
                // attrs: lat, lon
                // child nodes: ele, time, name, desc, sym, type
                break;
            case 'rte': // route
                $this->currentType = 'rte';
                $this->currentItem = array();
                // child nodes: name, desc, number, rtept's
                break;
            case 'trk': // track
                $this->currentType = 'trk';
                $this->currentItem = array();
                // child nodes: name, desc, number, topografix:color
                break;
            case 'rtept': // route point
                $this->currentType = 'rtept';
                // attrs: lat, lon
                // child nodes: ele, time, name, cmt, desc, sym, type
                break;
            case 'trkseg': // track segment
                $this->currentType = 'trkseg';
                // no attrs; trkpt children
                break;
            case 'trkpt': // track point
                 $this->currentType = 'trkpt';
               // attrs: lat, lon
                // child nodes: ele, time, sym
                break;
        }
    }
    
    function _characterData( $parser, $data )
    {
        if( $this->element != '' )
        {
            $text = trim( $data );
            
//            print $this->element . '=' . $text . '<br/>';
            
            // do something
            if( is_array($this->currentItem) )
            {
//            print $this->element . '=' . $text . '<br/>';
                $this->currentItem[ $this->element ] = $text;
            }
            
            $this->element = '';
        }
    }
    
    function _endElement( $parser, $name )
    {
        $this->path = substr( $this->path, 0, -(1+strlen($name)) );
        switch( $name )
        {
            case 'wpt':
                $this->currentItem = null;
                break;
            case 'rte':
                $this->routes[ $this->currentItem['name'] ] = $this->currentItem;
                $this->currentItem = null;
                break;
            case 'trk':
                $this->tracks[ $this->currentItem['name'] ] = $this->currentItem;
                $this->currentItem = null;
                break;
        }
    }
}

//$gpx = new GPX();
//$gpx->load( 'd:/share/drupal-4.7.0-beta4/files/fells_loop.gpx' );
//$gpx->parse();

?>
