<?php

/**
 * Notes: this widget needs php_xsl.dll extension.
 */

if( isset($_SERVER['QUERY_STRING'])
&& $_SERVER['SCRIPT_FILENAME'] == str_replace("\\","/",__FILE__) )
{
    /**
     * this part is executed on calls from javascript
     */
    // save current directory and change to drupal dir
    $dir = getcwd();
    $drupaldir = substr( dirname( __FILE__ ), 0, strrpos(dirname( __FILE__ ),"modules") );
    chdir( $drupaldir );
    
    // bootstrap to drupal session information
    require_once './includes/bootstrap.inc';
    drupal_bootstrap( DRUPAL_BOOTSTRAP_FULL );
    
    // restore current directory
    chdir( $dir );
//    print $drupaldir . '<br/>';

    // functionality
    parse_str( $_SERVER['QUERY_STRING'], $params );
    if( isset( $params['action'] ) )
    {
        $nid = $params['nid'];
        
        $filename = $params['filename'];
        $rec = db_fetch_array( db_query( "select filepath from files"
            . " where nid = %d and filename = '%s'", $nid, $filename ) );
        $filepath = $rec['filepath'];
        $gpxfile = $drupaldir . $filepath;
        
//        global $user, $conf;
//        print var_export($user,true) . '<br/>';
//        print var_export($conf,true) . '<br/>';
//        print var_export($_SESSION,true) . '<br/>';
        
        switch( $params['action'] )
        {
            case 'gettracks':
                // return tracks using gettracks.xsl
                $xsltfile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'gettracks.xsl';
                
                $domXml = new DOMDocument();
                $domXml->validateOnParse = true;
                $domXml->load( $gpxfile );
                
                $domXslt = new DOMDocument();
                $domXslt->validateOnParse = true;
                $domXslt->load( $xsltfile );
                
                $proc = new XsltProcessor();
                $proc->registerPhpFunctions();
                $xsl = $proc->importStylesheet($domXslt);
                $newdom = $proc->transformToDoc($domXml);
                echo $newdom->saveHTML();
                break;
                
            case 'getroutepoints':
                // return points for specific track
                $xsltfile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'getroutepoints.xsl';
                
                $domXml = new DOMDocument();
                $domXml->validateOnParse = true;
                $domXml->load( $gpxfile );
                
                $domXslt = new DOMDocument();
                $domXslt->validateOnParse = true;
                $domXslt->load( $xsltfile );
                
                $proc = new XsltProcessor();
                $proc->registerPhpFunctions();
                $xsl = $proc->importStylesheet($domXslt); // attach the xsl rules
                $proc->setParameter( '', 'routenumber', $params['route'] );
                $newdom = $proc->transformToDoc($domXml);
                echo $newdom->saveHTML();
                break;
                
            case 'gettrackpoints':
                // return points for specific track
                $xsltfile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'gettrackpoints.xsl';
                
                $domXml = new DOMDocument();
                $domXml->validateOnParse = true;
                $domXml->load( $gpxfile );
                
                $domXslt = new DOMDocument();
                $domXslt->validateOnParse = true;
                $domXslt->load( $xsltfile );
                
                $proc = new XsltProcessor();
                $proc->registerPhpFunctions();
                $xsl = $proc->importStylesheet($domXslt); // attach the xsl rules
                //$proc->setParameter( '', 'tracknumber', $params['track'] );
                $proc->setParameter( '', 'track', $params['track'] );
                $newdom = $proc->transformToDoc($domXml);
                echo $newdom->saveHTML();
                break;
                
            case 'getwaypoints':
                // return waypoints
                $xsltfile = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'getwaypoints.xsl';
                
                $domXml = new DOMDocument();
                $domXml->validateOnParse = true;
                $domXml->load( $gpxfile );
                
                $domXslt = new DOMDocument();
                $domXslt->validateOnParse = true;
                $domXslt->load( $xsltfile );
                
                $proc = new XsltProcessor();
                $proc->registerPhpFunctions();
                $xsl = $proc->importStylesheet($domXslt);
                $newdom = $proc->transformToDoc($domXml);
                echo $newdom->saveHTML();
                break;
        }
    }
//    global $user;
//    echo str_replace( "\n", "<br/>", var_export($user,true) );
//    echo str_replace( "\n", "<br/>", var_export($_SERVER,true) );
    exit();
}

if ( ! defined( 'MAPPINGWIDGETS_DIR' ) )
{
    define( 'MAPPINGWIDGETS_DIR', 
      substr( dirname( __FILE__ ), 0, strrpos(dirname( __FILE__ ),"widgets") )
      . 'lib/mappingwidgets/' );
}

require_once( MAPPINGWIDGETS_DIR . 'widgets/Base.php' );
require_once( 'GPX.php' );

class CartoGPXFiles extends Base
{
    var $files = array();
    var $nid;
    var $vid;
    
    function CartoGPXFiles( $params )
    {
        $this->Base( $params );
        
        // drupal things here?
        global $user, $conf, $locale, $queries;
        
        // get node id (Note: statement copied from search module)
        $path = drupal_get_normal_path( $_GET['q']) ;
        
        // When viewing revisions: e.g. $_SERVER['QUERY_STRING']='q=node/2/revisions/2/view'
        // with $_GET['q']='node/2'
        
        $regexpq = '!q=(?:node|book)/(?:view/)?([0-9]+)/(?:revisions/)?([0-9]+)!i';
        $regexpn = '!(?:node|book)/(?:view/)?([0-9]+)!i';
        if( ! preg_match( $regexpq, $_SERVER['QUERY_STRING'], $match ) )
            preg_match( $regexpn, drupal_get_normal_path( $_GET['q']), $match );

        if( count($match) > 0 )
        {
            $this->nid = $match[1];
            $this->vid = $match[2];
            if( $this->nid > 0 )
            {
                if( isset($this->vid) )
                {
                    $query = "SELECT n.nid, n.vid, v.description, f.fid, f.filename, f.filepath
                        FROM node_revisions n
                        INNER JOIN files f ON f.nid = n.nid
                        INNER JOIN file_revisions v ON v.fid = f.fid
                        WHERE v.vid = n.vid
                        AND n.nid = %d
                        AND n.vid = %d";
                    $result = db_query( $query, $this->nid, $this->vid );
                }
                else
                {
                    $query = "SELECT n.nid, n.vid, v.description, f.fid, f.filename, f.filepath
                        FROM node n
                        INNER JOIN files f ON f.nid = n.nid
                        INNER JOIN file_revisions v ON v.fid = f.fid
                        WHERE v.vid = n.vid AND n.nid = %d";
                    $result = db_query( $query, $this->nid );
                }
                
                while( ($row = db_fetch_array( $result)) )
                {
                    // todo: do something with filepath?
                    //$filepath = file_create_path($row['filepath']);
                    
                    // add to list of files
                    $this->files[ $row['filename'] ] = 
                        array( 'description' => $row['description']
                        , 'filepath' => $row['filepath'] );
                    
                    $gpx = new GPX( );
                    $gpx->load( $row['filepath'] );
                    $gpx->parse( );
                    $gpx->free( );
                    continue;
                    
                    $dom = new DOMDocument();
                    $dom->validateOnParse = true;
                    $dom->load( $row['filepath'] );
        /*
                    print 'documentElement: ' . $dom->documentElement->tagName . "<br/>";
                    print 'xmlEncoding: ' . $dom->xmlEncoding . '<br/>';
                    print 'validateOnParse: ' . $dom->validateOnParse . '<br/>';
                    
                    // bounds
                    $boundsNode = $dom->documentElement->getElementsByTagName( 'bounds' )->item(0);
                    $bbox = array( $boundsNode->getAttribute( 'minlon' )
                        , $boundsNode->getAttribute( 'minlat' )
                        , $boundsNode->getAttribute( 'maxlon' )
                        , $boundsNode->getAttribute( 'maxlat' )
                        );
                    print 'bbox: ' . serialize($bbox) . "<br/>";
        */
                    $xpath = new DOMXPath( $dom );
                    $query = '/wpt/*';
                    $nodes = $xpath->query( $query );
                    print 'xpath: ' . $query . '; lengte: ' . $nodes->length . '<br/>';
                    foreach( $nodes as $node )
                    {
                        print $node->tagName . ':' . serialize( $node ) . "<br/>";
                    }
                    
                    // way points
                    $nodes = $dom->getElementsByTagName( 'wpt' );
                    print $nodes->length . ' waypoints<br/>';
                    // routes
                    $nodes = $dom->getElementsByTagName( 'rte' );
                    print $nodes->length . ' routes<br/>';
                    // tracks (name|desc|number|topografix:color)
                    $nodes = $dom->getElementsByTagName( 'trk' );
                    print $nodes->length . ' tracks<br/>';
                    // $nodes = $dom->getElementsByTagName( 'wpt' );
    /*
                $nodes = $dom->documentElement->childNodes;
                foreach( $nodes as $node )
                {
                    print $node->tagName;
                    if( $node->nodeType == XML_ELEMENT_NODE )
                    {
                        $names = $node->getElementsByTagName('name');
                        if( $names->length > 0 )
                            print ':"' . $names->item(0)->textContent . '"';
                        print '<br/>';
                    }
                }
            */
                }
            }
        }
    }
    
    /**
     * get css classes
     */
    function getCSSClasses()
    {
        return parent::getCSSClasses() . " " . strtolower(__CLASS__);
    }
    
    /**
     * get inner body of html representation
     */
    function getInnerBody()
    {
        $body = "<select id=\"{$this->id}-select\" style=\"width:240px\">\n";
        $body .= "<option value=\"0\" selected>(select a tracklog)</option>\n";
        foreach( $this->files as $filename => $rec )
        {
            $body .= "<optgroup label=\"{$rec['description']}\" />\n";

            // todo: add waypoints and/or tracks
            $gpx = new GPX( );
            $gpx->load( $rec['filepath'] );
            $gpx->parse( );
            $gpx->free( );
            
            //$body .= "<!-- tracks = " . var_export($gpx->tracks,true) . " -->";
            //$body .= "<!-- routes = " . var_export($gpx->routes,true) . " -->";
            
            // waypoints
            $body .= "<option class=\"gpx-option\" value=\"{$filename}|waypoints\">waypoints of {$rec['description']}</option>\n";
            // tracks (sorted)
            ksort( $gpx->tracks );
            //$body .= "<!-- " . var_export($gpx->tracks,true) . " -->\n";
            foreach( $gpx->tracks as $number => $track )
            {
                $body .= "<option class=\"gpx-option\" value=\"{$filename}|trackpoints|{$number}\">track #{$number} of {$rec['description']}</option>\n";
            }
            // routes (sorted)
            ksort( $gpx->routes );
            foreach( $gpx->routes as $number => $route )
            {
                $body .= "<option class=\"gpx-option\" value=\"{$filename}|routepoints|{$number}\">route #{$number} of {$rec['description']}</option>\n";
            }
        }
        $body .= "</select>\n";
        return $body;
    }
    
    /**
     * get javascript init code
     */
    function getJsInitCode()
    {
        // get standard init for widget
        $output = parent::getJsInitCode();
        // additional init
        if( $this->nid )
            $output .= "\nvar gpx_nid = {$this->nid};\n";
        $output .= "var gpx_bbox = [];\n";
        foreach( $this->files as $filename => $rec )
        {
            $gpx = new GPX( );
            $gpx->load( $rec['filepath'] );
            $gpx->parse( );
            $gpx->free( );

            $output .= "gpx_bbox['$filename'] = '$gpx->bbox';\n";
//            $output .= "/* " . var_export( $gpx, true ) . " */";
        }
        return $output;
    }
}
//print __FILE__ . '<br/>';
//print $_SERVER['SCRIPT_FILENAME'] . '<br/>';

?>
