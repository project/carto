<?php

/**
 * These are the functions to be defined only when module is enabled.
 * Otherwise they could mess up with the admin system
 */

$carto_config_path = drupal_get_path('module','carto') . '/config.inc';
if( file_exists( $carto_config_path ) )
{
    include $carto_config_path;
    
    // override carto variables
    if( defined( 'SMARTY_DIR' ) )
        variable_set( 'carto_smarty_dir', SMARTY_DIR );
    if( defined( 'MAPPINGWIDGETS_DIR' ) )
        variable_set( 'carto_mappingwidgets_dir', MAPPINGWIDGETS_DIR );
    if( defined( 'MAPPINGWIDGETS_BASE_URI' ) )
    {
        $value = MAPPINGWIDGETS_BASE_URI;
        if( strpos( $value, base_path() ) === 0 )
        {
            // remove base_path from MAPPINGWIDGETS_BASE_URI
            $value = substr( $value, strlen(base_path()) );
        }
        variable_set( 'carto_mappingwidgets_base_uri', $value );
    }
}

if( ! defined('SMARTY_DIR') )
{
    $value = variable_get('carto_smarty_dir','');
    if( file_exists( $value . 'Smarty.class.php' ) )
    {
        define( "SMARTY_DIR", $value );
    }
    else if( is_dir( dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR ) )
    {
        // smarty installed under carto module
        define( "SMARTY_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR );
    }
    else
    {
        // default (for Debian Linux)
        define( 'SMARTY_DIR', '/usr/share/php/smarty/libs/' );
    }
}

if( ! defined('MAPPINGWIDGETS_DIR') )
{
    $value = variable_get('carto_mappingwidgets_dir','');
    if( file_exists( $value . 'MappingWidgets.php' ) )
    {
        define( "MAPPINGWIDGETS_DIR", $value );
    }
    else if( is_dir( dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'mappingwidgets' . DIRECTORY_SEPARATOR ) )
    {
        // mappingwidgets installed under carto module
        define( "MAPPINGWIDGETS_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'mappingwidgets' . DIRECTORY_SEPARATOR );
    }
    else
    {
        // default (for Debian Linux)
        define( "MAPPINGWIDGETS_DIR", "/usr/local/share/mappingwidgets/" );
    }
}

if( ! defined('MAPPINGWIDGETS_BASE_URI') )
{
    $value = variable_get('carto_mappingwidgets_base_uri','');
    global $base_url;
    if( $value != '' )
    {
        define( "MAPPINGWIDGETS_BASE_URI", base_path() . $value );
    }
    else
    {
        define( 'MAPPINGWIDGETS_BASE_URI', base_path() . 'modules/carto/lib/mappingwidgets/htdocs/' );
    }
}

if( file_exists(variable_get('carto_mappingwidgets_dir',MAPPINGWIDGETS_DIR) . 'MappingWidgets.php') 
 && file_exists(variable_get('carto_smarty_dir',SMARTY_DIR) . 'Smarty.class.php' ) )
{
    // load configuration for current Drupal site
    if( ($path = drupal_get_filename( 'config', 'carto' )) )
        require_once( $path );
    
    require_once( variable_get('carto_mappingwidgets_dir',MAPPINGWIDGETS_DIR) . 'MappingWidgets.php' );
    
    class CartoMapper extends SmartyMapping
    {
        function CartoMapper( )
        {
            // use current drupal locale
            global $locale;
            
            $this->SmartyMapping( $locale );
            
            // register selected carto widgets
            if( file_exists( dirname(__FILE__) . '/widgets/' ) )
            {
                $count = $this->registerWidgetPath( dirname(__FILE__) . '/widgets/' );
            //print 'register count=' . $count . '<br/>';
            }
            
            // add carto skin (name, path, and uri)
            $this->addSkin( 'carto'
                , dirname(__FILE__) . 'skin'
                , base_path() . 'modules/carto/skin/' );
            
            $this->caching = (variable_get('carto_enable_smarty_caching','1') == '1' ? true : false);
            $this->force_compile = (variable_get('carto_smarty_force_compile','0') == '1' ? true : false);
/* *
            print 'carto info | locale=' . $locale 
                . ' | path=' . drupal_get_path_alias($_GET['q'])
                . ' | caching=' . $this->caching
                . '<br />'
                . MAPPINGWIDGETS_COMPILE_DIR
                ;
/* */
            $this->set_html_head( );
            
            // register the resource name "carto"
            $this->register_resource( "carto"
                , array( "carto_get_template",
                         "carto_get_timestamp",
                         "carto_get_secure",
                         "carto_get_trusted" ) );
        }
        
        function set_html_head( )
        {
            // todo: put this somewhere else in drupal 5.0?
            drupal_add_css( variable_get('carto_mappingwidgets_base_uri',substr(MAPPINGWIDGETS_BASE_URI,strlen(base_path())))
                    . 'skins/default/styles/mappingwidgets.css' );
            drupal_add_css( drupal_get_path('module','carto') . "/carto.css" );

            $output = "\n";
            $output .= "<meta http-equiv=\"imagetoolbar\" content=\"no\" />\n";
            return drupal_set_html_head( $output );
        }
        
        /**
         * To be called by Smarty output filter to translate
         * mappingwidgets codes into texts for current locale.
         * NB: mappingwidgets keys in drupal's locale tables 
         * are prefixed by '#'.
         */
        function getTranslation( $key )
        {
            $text = t('#' . $key);
            if( $text != '#' . $key )
            {
                return $text;
            }
            else
            {
                // try to get value from mappingwidgets
                return $GLOBALS['_NG_LANGUAGE_']->getTranslation( $key );
            }
        }
        
    } // class CartoMapper
    
    function get_carto_mapper( )
    {
        if( ! isset( $GLOBALS['cartoSmarty'] ) )
        {
            $GLOBALS['cartoSmarty'] = & new CartoMapper();
        }
        return $GLOBALS['cartoSmarty'];
    }
}

function carto_get_template( $tpl_name, &$tpl_source, &$smarty_obj )
{
    // do database call here to fetch your template,
    // populating $tpl_source
    $sql = "select content from {carto_filter_blocks} where cfid = '%s'";
    $result = db_query( $sql, $tpl_name );
    if( db_num_rows( $result ) > 0 )
    {
        $item = db_fetch_object( $result );
        $tpl_source = $item->content;
        return true;
    }
    else
    {
        return false;
    }
}

function carto_get_timestamp( $tpl_name, &$tpl_timestamp, &$smarty_obj )
{
    // do database call here to populate $tpl_timestamp.
    $sql = "select time from {carto_filter_blocks} where cfid = '%s'";
    $result = db_query( $sql, $tpl_name );
    if( db_num_rows( $result ) > 0 )
    {
        $item = db_fetch_object( $result );
        $tpl_timestamp = $item->time;
        return true;
    }
    else
    {
        return false;
    }
}


function carto_get_secure( $tpl_name, &$smarty_obj )
{
    // assume all templates are secure
    return true;
}


function carto_get_trusted( $tpl_name, &$smarty_obj )
{
    // not used for templates
}

?>
