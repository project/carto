INSTALLATION OF CARTO MODULE

step 1

Install the necessary software: smarty (from http://smarty.php.net) and
mappingwidgets (from http://mappingwidgets.sourceforge.net).

step 2

Install the carto module in ../modules/carto/.

step 3

Enable the carto module in drupal.

step 4

Configure the carto module in admin/settings/carto. Specify the correct
paths for the mappingwidgets and smarty packages. If mappingwidgets installed
only for use with carto, install it in modules/carto/lib/mappingwidgets.

Copy config.inc.dist to config.inc and set the correct file paths for 
your system.

step 5

Add new input format in the 'admin/settings/filters' page: 
call it e.g. 'Inline mappingwidgets' and configure it to use inline 
mappingwidgets. It is best to arrange that the HTML filter is also enabled for
this input format and that it is used before the carto filter (Rearrange).
Enable the input format for authenticated users.

step 6

If desired, enable the overview and/or legend block to show in the left 
(preferred) or right column.

step 7

Test everything by making a node using the new filter.
For the body use something like the following:

{carto-filter}
    {mappingwidget type="ZoomIn" factor=0.5 target="next"
      normalimage="buttons/button_zoomin_1.png"
      hoverimage="buttons/button_zoomin_2.png"
      selectedimage="buttons/button_zoomin_3.png"
      group="navigation"}
    {mappingwidget type="ZoomOut" factor=0.5 target="next" 
      group="navigation"}
    {mappingwidget type="Move" group="navigation" target="next"}
    {mappingwidget type="Query" group="navigation" target="next"}
    {mappingwidget type="Ruler" group="navigation" target="next"}
    
    {mappingwidget type="ZoomFull" target="next"}
    {mappingwidget type="ZoomFirst" target="next"}
    {mappingwidget type="ZoomPrevious" target="next"}
    {mappingwidget type="ZoomNext" target="next"}
    {mappingwidget type="ZoomLast" target="next"}
{/carto-filter}

{carto-filter}
    {mappingwidget type="SimpleMap" width="480" height="240" 
     service="http://www2.demis.nl/wms/wms.asp?wms=WorldMap"
     layers="Bathymetry,Topography,Borders,Ocean%20features,Hillshading,Waterbodies,Rivers,Streams,Railroads,Highways,Builtup areas"
     query_layers="Bathymetry,Topography,Borders,Ocean%20features,Waterbodies,Rivers,Streams,Railroads,Highways,Builtup areas"
     extent="-180,-90,180,90" srs="epsg:4326"
     format="image/png"
     }
{/carto-filter}

Note that the legend block only functions with a UMN Mapserver thus not
with the one of this example.

