
--
--
--

create table carto_filter_nodes(
 nid integer NOT NULL default '0'
, numblocks integer NOT NULL default '0'
, PRIMARY KEY(nid)
);

--
--
--

create table carto_filter_blocks(
 cfid varchar(254) unique not null default ''
, crc integer
, time integer
, content text
, PRIMARY KEY(cfid)
);

--
--
--

create table carto_wms_node(
    nid integer not null
,   name varchar(32)
,   service varchar(255)
,   capabilities text
,   bbox varchar(255)
,   layers varchar(255)
,   abstract text
,   keywords varchar(255)
,   title varchar(255)
,   captime integer NOT NULL default '0'

,   PRIMARY KEY( nid )
);

--
--
--

create table carto_wms_layer(
    nid integer not null
,   lid integer not null
,   name varchar(255)
,   title varchar(255)
,   abstract text
,   minx double precision
,   miny double precision
,   maxx double precision
,   maxy double precision
,   queryable smallint default '0'
,   opaque smallint default '0'
,   cascaded smallint default '0'

,   PRIMARY KEY( nid, lid )
);

--
--
--

create table carto_wms_layer_properties(
    nid integer not null
,   lid integer not null
,   property varchar(255) not null
,   name varchar(255) not null
,   value text

,   PRIMARY KEY( nid, lid, property, name )
);

